import Guest from './components/guest/Guest';
import Header from './components/header/Header';
import Topbar from './components/topbar/Topbar';
import Background from './components/background/Background'; 
import Shema from './components/shema/Shema';
import MyMenu from './components/myMenu/MyMenu';
import Slide from './components/slider/Slide';

function App() {
  return (
    <div className="App">
      <Topbar />
      <Header />
      <Guest />
      <Background />
      <Shema />   
      <MyMenu />  
      <Slide /> 
    </div>
  );
}

export default App;
