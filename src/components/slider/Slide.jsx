import { useState } from 'react'
import './slide.scss'

function Slide() {
  const [currentSlider, setCurrentSlider] = useState(2)

  const data = [
    {
      id: 1,
      img: "./assets/img/slide-bg.jpg"
    },
    {
      id: 2,
      img: "./assets/img/slide-bg.jpg"
    },
    {
      id: 3,
      img: "./assets/img/slide-bg.jpg"
    }
  ]

  const handleClick = (way) => {
    way === 'left' 
      ? setCurrentSlider(currentSlider > 0 ? currentSlider - 1 : data.length - 1)
      : setCurrentSlider(currentSlider < data.length - 1 ? currentSlider + 1 : 0)
  }

  return (
    <div className='slide' id='slide'>
      <div className="container">
        <div className="slider" style={{transform: `translateX(-${currentSlider * 100}vw)`}}>
          {data.map((d)=>(                    
            <div className="slide-wrapper" key={d.id}>
              <div className="slide">
                <img src={d.img} alt="slide-bg" />
              </div>
            </div>
          ))}           
        </div>        
      </div> 
      <img 
        src="assets/img/arrow.png" 
        className='arrow left' 
        alt="arrowLeft"
        onClick={() => handleClick('left')}
      />
      <img 
        src="assets/img/arrow.png" 
        className='arrow right' 
        alt="arrowRight" 
        onClick={() => handleClick()}
      />     
    </div>
  )
}

export default Slide