import Menu from '../menu/Menu';
import './topbar.scss';

function Topbar() {
  return (
    <div className='topbar'>
      <div className="container">
        <div className="top-line">
          <div className="logo-wrapper">
            <img src="assets/img/logo.svg" alt="logo" />
          </div>

          <div className="top-info">
            <div className="adress">
              <div className="icon-wrap">
                <img src="assets/img/icons/adress.png" alt="adress-icon" />
              </div>
              <span className="adress-text">г. Ставрополь <br /> ул. Пушкина 272</span>    
            </div>

            <div className="icons">
              <div className="icons-wrap">
                <img src="assets/img/icons/email.png" alt="email-icon" />
              </div>
              <div className="icons-wrap">
                <img src="assets/img/icons/phone.png" alt="phone-icon" />
              </div>
            </div>

            <div className="phone">
              <span className='phone-number'>+7 (123) 45-67-89</span>
              <span className='phone-text'>Перезвоните мне</span>
            </div>
          </div>
        </div>

        <Menu />
      </div>
    </div>
  )
}

export default Topbar
