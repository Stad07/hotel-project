import './shema.scss';

function Shema() {
  return (
    <div className='shema'>
      <div className="container">
        <h2 className='shema-title'>Схема залов</h2>        
        <div className="shema-content">
          <span>Зал Первый</span>
          <span>Зал Второй</span>          
          <div className="left">
            <div className="image-wrapper">
              <img src="assets/img/shema.png" alt="shema" />
            </div>
          </div>
          <div className="right">
            <div className="shema-info">
              <h3 className='info-title'>Просторный зал с панорамным видом на город</h3>
              <div className='info-descr'>
                <div><span>площадь</span><span>160 кв. метров</span></div>
                <div><span>вместимость</span><span>до 100 человек</span></div>
                <div><span>фуршет</span><span>до 200 человек</span></div>
              </div>
              <div className="buttons">
                <button className='shema-btn btn'>Забронировать</button>
                <button className='shema-btn btn white'>Посмотреть галерею</button>
              </div>              
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Shema
