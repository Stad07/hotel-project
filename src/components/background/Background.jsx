import './background.scss';

function Background() {
  return (
    <div className='background' style={{backgroundImage: 'url(/assets/img/middle-bg.jpg)' }}>
      
    </div>
  )
}

export default Background
