import './header.scss';

function Header() {
  return (
    <div className="header">
      <div className="container">
        <div className="header-wrapper" style={{ backgroundImage: 'url(/assets/img/header-bg.jpg)' }}>
          <div className="header-left">
            <h1 className='header-text'>Изысканный свадебный зал <br/>с панорамным видом на город</h1>
          </div>
          <div className="header-right">
          <form className="form">
            <h3>Узнайте свободные даты и забронируйте зал</h3>
            <div className="form-group">
              <label htmlFor="name">Ваше имя</label>
              <input type="text" id="name" placeholder="Иван" />
            </div>
            <div className="form-group">
              <label htmlFor="phone">Ваш номер телефона</label>
              <input type="text" id="phone" placeholder="+7 (__) __-__-__" />
            </div>
            <button type="submit" className='form-btn btn'>Забронировать</button>
          </form>
          </div>
        </div>

        <div className="bottom-header">
          <div className="left">
            <h2>Секрет успешной свадьбы – <br />в спокойных и счастливых молодых</h2>
            <p>Мы сделаем все, чтобы вы чувствовали себя комфортно на собственной свадьбе, а гости вспоминали ее самым вкусными и веселым событием в жизни!</p>
          </div>
          <div className="right">
            <div className="header-image">
              <img src="assets/img/rings.png" alt="rings" />         
            </div>
            <div className="pigeon-image-left">
              <img src="assets/img/pigeon-left.png" alt="pigeon" />
            </div>
            <div className="pigeon-image-right">
            <img src="assets/img/pigeon-right.png" alt="pigeon" />
            </div>
          </div>
        </div>
      </div>      
    </div>
  )
}

export default Header
