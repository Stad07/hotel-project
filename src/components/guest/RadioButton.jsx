function RadioButton({label, value, onChange}) {
  return (
    <span class="quantity-ratio">
      <label>
        <input 
          name="quantity" 
          type="radio"
          value={label} 
          checked={value} 
          onChange={onChange}
        />
        {label}
      </label>
    </span>    
  )
}

export default RadioButton
