import { useState } from 'react';
import RadioButton from './RadioButton'
import './guest.scss';

function Guest() {
  const [value, setValue] = useState(false);

  const quantity = ['5-30', '30-60', '60-80', '80-100', '100-120']

  const handleChange = () => {
    setValue(!value)
  }

  return (
    <div className='guest'>
      <div className="container">
        <h2>Рассчитайте стоимость вашего банкета</h2>
        <div className="guest-price">
          <div className="left">
            <h4>Вопрос 1 из 5</h4>
            <div className='progress-line'></div>
            <h3>Количество гостей</h3>
            <div className="guest-quantity">
              {
                quantity.map((q, i) => (
                  <RadioButton 
                    key={i}
                    label={q}
                    onChange={handleChange}
                  />
                ))
              }              
            </div>
            <button className='guest-btn btn'>Следующий вопрос <span>&#8594;</span></button>           
          </div>          

          <div className="right" style={{ backgroundImage: 'url(/assets/img/group.svg)' }}>
            <div className="content-wrapper">
              <h3>Ответьте на 5 вопросов и получите горку <br/>из шампанского в подарок</h3>                            
            </div>
            <div className="image-wrapper">
              <img src="assets/img/bottle.png" alt="bottle" />
            </div>                    
          </div>
        </div>
      </div>      
    </div>
  )
}

export default Guest
