function MenuItem({ text }) {
  return (
    <li className="list-item">      
      <a href="/">{text}</a>
    </li>
  )
}

export default MenuItem
