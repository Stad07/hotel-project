import MenuItem from "./MenuItem";
import './menu.scss';

function Menu() {
  const menuItems = [
    'Расчёт стоимости',
    'О нас',
    'Выездная регистрация',
    'Схемы залов',
    'Меню',
    'Галерея',
    'Отзывы',
    'Контакты'
  ]

  return (
    <div className="menu">
      <ul className="menu-list">
        {
          menuItems.map((item, index) => (
            <MenuItem 
              key={index}
              text={item}
            />
          ))
        }
      </ul>
    </div>    
  )
}

export default Menu
