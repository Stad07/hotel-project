import './myMenu.scss';

function MyMenu() {
  return (
    <div className='my-menu'>
      <div className="container">
        <div className="my-menu-wrapper">
          <div className="left">
            <h2 className='my-menu-title'>Индивидуальное меню для <span>Вашего праздника</span></h2>
            <div className="my-menu-content">
              <div className="content-left">
                <img src="assets/img/icons/bell.png" alt="bell" />
                <p>Составим меню под любые предпочтения</p>
              </div>
              <div className="content-right">
                <img src="assets/img/icons/glass.png" alt="bell" />
                <p>Можно со своим алкоголем или порекомендуем надёжных поставщиков</p>
              </div>
            </div>
            <div className="my-menu-form">
              <h3>Получите полное банкетное меню</h3>
              <div className="form-wrapper">
                <div className="form-group">
                  <label>Ваше имя</label>
                  <input type="text" placeholder='Иван'/>
                </div>  
                <div className="form-group">                
                  <label>Ваш номер телефона</label>
                  <input type="text" placeholder='+7 (__) __-__-__'/>
                </div>
                <button className='my-menu-btn btn'>Забронировать</button>
              </div>                                 
            </div>
          </div>
          <div className="right">
            <div className="content">
              <h3>Скачайте пример банкетного меню на 50 человек</h3>
              <button className='my-menu-btn btn'>Скачать пример <span>&#8595;</span></button>
            </div>            
            <div className="image-wrapper">
              <img src="assets/img/note.png" alt="note" />
            </div>
          </div>
        </div>        
      </div>
    </div>
  )
}

export default MyMenu
